package ru.tsc.vinokurov.tm;

import ru.tsc.vinokurov.tm.api.ICommandRepository;
import ru.tsc.vinokurov.tm.constant.ArgumentConst;
import ru.tsc.vinokurov.tm.constant.TerminalConst;
import ru.tsc.vinokurov.tm.model.Command;
import ru.tsc.vinokurov.tm.repository.CommandRepository;
import ru.tsc.vinokurov.tm.util.NumberUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Application {

    private static final String TASK_MANAGER_VERSION = "1.8.0";

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    private static void displayHelp() {
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    private static void displayCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void displayArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void displayAbout() {
        System.out.println("Developer: Aleksey Vinokurov");
        System.out.println("Email: avv96@yandex.ru");
    }

    private static void displayVersion() {
        System.out.printf("Version: %s\n", TASK_MANAGER_VERSION);
    }

    private static void displayErrorArgument(String arg) {
        System.err.printf("Error! %s is not valid argument!\n", arg);
    }

    private static void displayErrorCommand(String arg) {
        System.err.printf("Error! %s is not valid command!\n", arg);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void close() {
        System.exit(0);
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.COMMANDS:
                displayCommands();
                break;
            case TerminalConst.ARGUMENTS:
                displayArguments();
                break;
            case TerminalConst.HELP:
                displayHelp();
                break;
            case TerminalConst.VERSION:
                displayVersion();
                break;
            case TerminalConst.ABOUT:
                displayAbout();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                displayErrorCommand(command);
        }
    }

    private static void processCommand() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String command;
            System.out.print("Enter command: ");
            while ((command = reader.readLine()) != null) {
                processCommand(command);
                System.out.print("Enter command: ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.COMMANDS:
                displayCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                displayArguments();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                displayErrorArgument(arg);
        }
    }

    private static boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        for (String arg : args) {
            processArgument(arg);
        }
        return true;
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int processors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[INFO]");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void main(String[] args) {
        if (processArgument(args)) close();
        displayWelcome();
        processCommand();
    }

}
